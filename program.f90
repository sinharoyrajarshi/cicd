program prg1

IMPLICIT NONE
REAL*8 x,y,z,j1,j2,j3,c1,c2,c3

x=2.3
y=4.1
z=3.4
j1=0.23
j2=9.12
j3=2.57
c1=y*j3-z*j2
c2=z*j1-x*j3
c3=x*j2-y*j1
write(*,*) "c1 =", c1
write(*,*) "c2 =", c2
write(*,*) "c3 =", c3

end program prg1
